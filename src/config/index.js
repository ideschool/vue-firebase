export default {
  apiKey: 'xxxxxxxxxxxxxxxx',
  authDomain: 'xxxxx.firebaseapp.com',
  databaseURL: 'https://xxxxxx.firebaseio.com',
  projectId: 'xxxxxxx',
  storageBucket: 'xxxxxxxx.appspot.com',
  messagingSenderId: 'xxxxxxxxxx',
};
