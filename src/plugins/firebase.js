export class Firebase {
  constructor(config) {
    this._firebase = window.firebase.initializeApp(config);
  }

  isAuthenticated() {
    return this._firebase.auth().currentUser !== null;
  }

  passwordRegister(email, password) {
    if (email === '' || password === '') {
      throw new Error('Missing user credentails data');
    }
    return this._firebase.auth().createUserWithEmailAndPassword(email, password);
  }

  passwordAuthenticate(email, password) {
    if (email === '' || password === '') {
      throw new Error('Missing user credentails data');
    }

    return this._firebase.auth().signInWithEmailAndPassword(email, password);
  }

  loginWithGoogle() {
    const provider = new window.firebase.auth.GoogleAuthProvider();
    return this._firebase.auth().signInWithPopup(provider);
  }

  logout() {
    return this._firebase.auth().signOut();
  }
}

export const firebasePlugin = {
  install(Vue, config) {
    Vue.prototype.$firebase = new Firebase(config);
  },
};
